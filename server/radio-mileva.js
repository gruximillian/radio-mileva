Meteor.publish('theUsers', function () {
  return Users.find({}, {fields: {services: 0}}); // Ne znam da li ima ili nema smisla da se polje services ne salje...
});

Meteor.publish('messageLog', function () {
  var currentUserId = this.userId,
      currentUserName;

  if (currentUserId) {
    currentUserName = Users.findOne({_id: this.userId}).username
    console.log('publish: ', currentUserName);
    //console.log('pub2: ', ChatLogs.find({users: {$in: [currentUserName]}}).fetch());
    return ChatLogs.find({users: {$in: [currentUserName]}}); // Ulogovani korisnik moze da primi sve chat logove u kojima je ucestvovao
  }
});

Accounts.onCreateUser(function (options, user) {
  user.friends = [];
  user.chatLog = [];
  user.profile = {};
  user.friendRequestsFrom = [];
  user.friendRequestsTo = [];
  user.loggedIn = false;

// Ovo ce sluziti za stranicu gde ce user menjati svoje podatke u profilu,
// pozivace se iz nekog drugog metoda
  if (options.profile) {
    user.profile = options.profile;
    user.username = options.profile.username;
  }

  return user;
});

/************************************/
/*           User methods           */
/************************************/

Meteor.methods({
  addNewFriendRequests: function (newFriendName) {
    if (newFriendName) {
      console.log('New friend request: ' + newFriendName);
      Users.update({username: newFriendName}, {$addToSet: {friendRequestsFrom: Meteor.user().username}});
      Users.update({_id: Meteor.userId()}, {$addToSet: {friendRequestsTo: newFriendName}});
      //Users.update({username: 'Gruja'}, {$addToSet: {emails: {address: 'grujic@mail.com', verified: false}}}) // Primer dodavanja novog emaila
    } else {
      console.log('"' + newFriendName + '": No such user!');
    }
  },
  addFriends: function (friendsToAdd) {
    friendsToAdd.forEach(function (current, index, array) {
      var currentUserName = Meteor.user().username;
      Users.update({_id: Meteor.userId()}, {$addToSet: {'friends': current}, $pull: {'friendRequestsFrom': current}});
      Users.update({username: current}, {$addToSet: {'friends': currentUserName}, $pull: {'friendRequestsTo': currentUserName}});
    });
  },
  removeFriends: function (friendsToRemove) {
    friendsToRemove.forEach(function (current, index, array) {
      Users.update({_id: Meteor.userId()}, {$pull: {'friends': current}});
      Users.update({username: current}, {$pull: {'friends': Meteor.user().username}});
    });
  },
  cancelRequests: function (canceledRequests) {
    canceledRequests.forEach(function (current, index, array) {
      Users.update({_id: Meteor.userId()}, {$pull: {'friendRequestsTo': current}});
      Users.update({username: current}, {$pull: {'friendRequestsFrom': Meteor.user().username}});
    });
  },
  denyRequests: function (requestsToDeny) {
    requestsToDeny.forEach(function (current, index, array) {
      Users.update({_id: Meteor.userId()}, {$pull: {'friendRequestsFrom': current}});
      Users.update({username: current}, {$pull: {'friendRequestsTo': Meteor.user().username}});
    })
  },
  loginUser: function () {
    var userId = Meteor.userId();
    Users.update({_id: userId}, {$set: {loggedIn: true}});
  },
  logoutUser: function (userId) {
    Users.update({_id: userId}, {$set: {loggedIn: false}});
  }
});

/************************************/
/*           Chat methods           */
/************************************/

Meteor.methods({
  initiateChat: function (chatObject) {
    var currentUser = Meteor.user().username,
        userId = Meteor.userId(),
        usersToInitiate = chatObject.users.slice(1),
        chatId = chatObject._id;
    //console.log(currentUser, 'is initating chat with: ', usersToInitiate);
    console.log('chatObject: ', chatObject);
    console.log('chatObject users: ', chatObject.users);
    console.log('chatId: ', chatId);

    ChatLogs.insert(chatObject);
    Users.update({_id: userId}, {$addToSet: {chatLog: chatId}});
    usersToInitiate.forEach(function (current) {
      console.log('users to initiate: ', current);
      Users.update({username: current}, {$addToSet: {chatLog: chatId}});
    });
  },
  logMessage: function (messageObject) {
    console.log('Logging message!');
    console.log('messageObject: ', messageObject);
    var chatId = messageObject.chatId;
    console.log('chatId: ', chatId);
    ChatLogs.update({_id: chatId}, {$addToSet: {'messages': messageObject}})
  },
  endChat: function (chatId) {
    ChatLogs.update({_id: chatId}, {$set: {endedAt: new Date()}});
  }
});
