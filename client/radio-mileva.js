Accounts.ui.config({
  passwordSignupFields: 'USERNAME_AND_EMAIL'
});

Meteor.subscribe('theUsers');
Meteor.subscribe('messageLog');

Accounts.onLogin(function () {
  Session.set('loggedUser', Meteor.userId());
  Session.set('chatWith', []);
  //console.log('Logged in');
  Meteor.call('loginUser');
  Meteor.logoutOtherClients(); // Korisnik moze biti ulogovan u samo jednom klijentu
});

window.onbeforeunload = function () {
  Meteor.call('logoutUser', Session.get('loggedUser'));
  Session.set('chatWith', []);
  Meteor.logout();
}

/*
// Iz paketa gwendall:auth-client-callbacks, trenutno iskljucen, a koristim event handler dole za ovaj zadatak
Accounts.onLogout(function () {
  console.log('Logged out');
  Meteor.call('logoutUser', Session.get('loggedUser'));
});
*/

Template.body.events({
  'click #login-buttons-logout': function () {
    /* Bolje mi radi od gornjeg .onLogout metoda */
    //console.log('Logged out');
    Meteor.call('logoutUser', Session.get('loggedUser'));
  },
  'click .friends-list .friend': function (e) {
    var i,
        chatWith = Session.get('chatWith'),
        newFriendInChat = e.target.innerHTML,
        chatInitiated = false,
        currentUser = Meteor.user().username,
        date = new Date().valueOf(),
        chatId = date + '-' + currentUser + '_' + newFriendInChat + '-' + Math.floor(Math.random() * 10e12); // chatId se konstruise iz tri dela. Prvo, unix datum, zatim niz ucesnika u chatu i na kraju random broj

    function isInChat (current) {
      return current.name === newFriendInChat;
    }

    if (chatWith.some(isInChat)) {chatInitiated = true};

    if (!chatInitiated) {
      chatWith.push({name: newFriendInChat, chatId: chatId});
      Session.set('chatWith', chatWith);
    }

    $('#activate-' + newFriendInChat).click(); // Nece da aktivira tab na prvi klik, ali kasnije hoce, nije timing issue, elementi postoje...
    //Session.set('activeChat', chatid); // Kada bi prethodna linija radila na prvi klik, ovo bi bilo idealno resenje za pracenje aktivnog chata
  }
});

Template.body.helpers({
  friendsInChat: function () {
    var user = Meteor.user(),//Users.findOne({_id: Meteor.userId()}),
        currentUser = user.username,
        chatLogs = user.chatLog, // Niz _id-a za chat logove,
        chatWith = Session.get('chatWith'),
        friendsInChat = chatWith.map(function (current) {
          return current.name; // Lista imena korisnika sa kojima je chat iniciran
        });

    if (chatLogs) {
      chatLogs.forEach(function (currentChatId) {
        var log = ChatLogs.findOne({_id: currentChatId}),
            currentLogEnded = log.endedAt,
            newFriendInChat = log.users.filter(function (current) {
              //console.log('current: ', current, '; currentUser: ', currentUser);
              return current !== currentUser; // Ne zelimo da korisnik samog sebe stavi u listu chatova
            }).join(''); // Trenutno je samo jedan sagovornik prisutan u chatu, ali mozda ubuduce bude vise njih pa su zato u nizu
        if (currentLogEnded === 'undefined' && friendsInChat.indexOf(newFriendInChat) === -1) {
          // Vraca samo logove koji nisu okoncani i gde sagovornik vec nije u aktivnom chatu
          chatWith.push({name: newFriendInChat, chatId: currentChatId});
          Session.set('chatWith', chatWith);
          friendsInChat.push(newFriendInChat);
        };
      });
    }
    return friendsInChat;
  }
});

Template.friendsList.helpers({
  userFriends: function () {
    return Users.findOne({_id: Meteor.userId()}).friends;
  }
});

Template.friendsList.events({
  'click button': function (e) {
    e.preventDefault();
    var friendsToRemove = [];
    friendsToRemove.push(e.target.value);
    console.log('Remove!', friendsToRemove);
    Meteor.call('removeFriends', friendsToRemove);
  }
});

Template.acceptFriendRequests.helpers({
  displayRequestsFrom: function () {
    return Users.findOne({_id: Meteor.userId()}).friendRequestsFrom;
  }
});

Template.acceptFriendRequests.events({
  'click .add': function (e) {
    e.preventDefault();
    var acceptedFriends = [];
    acceptedFriends.push(e.target.value);
    console.log('Accepted friends: ' + acceptedFriends);
    Meteor.call('addFriends', acceptedFriends);
  },
  'click .remove': function (e) {
    e.preventDefault();
    var requestsToDeny = [];
    requestsToDeny.push(e.target.value);
    console.log('Denying: ', requestsToDeny);
    Meteor.call('denyRequests', requestsToDeny);
  }

});

Template.searchForFriends.events({
  'submit form': function (e) {
    e.preventDefault();
    var requested = e.target.requestedFriend.value.trim(),
        requestedFriend = Users.findOne({$or: [{username: requested}, {'emails.address': requested}]}),
        requestedFriendName,
        currentUser = Meteor.user(),
        currentUserEmails = [],
        currentUserFriends = currentUser.friends,
        currentUserRequestsFrom = currentUser.friendRequestsFrom;

    currentUserEmails = currentUser.emails.map(function (current, index, array) {
      return current.address; // Izvlacim samo adrese, bez polja 'verified'
    });

    if (requestedFriend) {
      requestedFriendName = requestedFriend.username;
      if (requestedFriendName === currentUser.username) {
        console.log('You cannot add yourself as a friend!');
      } else if (currentUserFriends.indexOf(requestedFriendName) !== -1) {
        console.log(requestedFriendName + ' is already your friend!');
      } else if (currentUserRequestsFrom.indexOf(requestedFriendName) !== -1) {
        var friendArray = [];
        friendArray.push(requestedFriendName);
        Meteor.call('addFriends', friendArray);
        console.log(requestedFriendName + ' already sent you a friend request, automatically adding as a friend.');
      } else {
        Meteor.call('addNewFriendRequests', requestedFriendName);
        console.log('Friend request sent to ' + requestedFriendName);
      }
    } else if (requested) {
      console.log('Korisnik ', requested, ' ne postoji u bazi!');
    } else {
      console.log('Upisite ime koje trazite!');
    }

    e.target.requestedFriend.value = '';

/*Test kod: Pre svega primer za upotrebu callback funkcije
    Meteor.call('getUserData', function (error, result) {
      var friends = result.friends,
          username = result.username;
      console.log('User ' + username + ' has following friends: ' + friends);
    });
*/
  }
});

Template.friendRequestsTo.helpers({
  displayRequestsTo: function () {
    return Users.findOne({_id: Meteor.userId()}).friendRequestsTo;
  }
});

Template.friendRequestsTo.events({
  'click .remove': function (e) {
    e.preventDefault();
    var requestsToRemove = [];
    requestsToRemove.push(e.target.value);
    console.log('Remove!', requestsToRemove);
    Meteor.call('cancelRequests', requestsToRemove);
  }
});

Template.chatTabName.events({
  'click a': function (e) {
    e.preventDefault();
    $(e.target).tab('show');
    Session.set('activeChat', e.target.dataset.chatid);
    console.log('activeChat: ', Session.get('activeChat'));
  },
  'click a button': function (e) {
    e.preventDefault();
    var chatWith = Session.get('chatWith'),
        chatWithNames = chatWith.map(function (current) { return current.name; }),
        closeChatWith = chatWithNames.indexOf(e.target.value),
        chatId = chatWith.filter(function (current) {
          return current.name === e.target.value;
        })[0].chatId;
    //console.log('closing: ', chatId);
    Meteor.call('endChat', chatId);
    chatWith.splice(closeChatWith, 1);
    console.log('chatWith: ', chatWith);
    Session.set('chatWith', chatWith);
    e.stopPropagation();
  }
});

Template.chatTabPanel.events({
  'submit form': function (e, template) {
    e.preventDefault();
    var messageText = e.target.sendMessage.value.trim(),
        chatObject,
        messageObject = {},
        chatWithName = (e.target.sendMessage.id).split('-')[1],
        chatId = Session.get('activeChat'),
        chatIdComponents = chatId.split('-'),
        dateString = chatIdComponents[0] * 1, // Unix datum kada je zapocet chat
        startedAt = new Date(dateString),
        users = chatIdComponents[1].split('_'), // Niz ucesnika u chatu
        currentUser = Meteor.user().username,
        sentTo = users.filter(function (current) {
          return current !== currentUser; // Ucesnici kojima se salje poruka, trenutno samo jedan, mozda budem napravio i za multichat nekada (izbacuje currentUser-a)
        }),
        messagesInChat = ChatLogs.findOne({_id: chatId});

    if (messageText === '') return;

    messageObject.chatId = chatId;
    messageObject.createdAt = new Date();
    messageObject.from = currentUser;
    messageObject.to = sentTo;
    messageObject.content = messageText;

    if (!messagesInChat) { // Inicijalizacija chata
      chatObject = {};
      chatObject._id = chatId;
      chatObject.startedAt = startedAt;
      chatObject.endedAt = 'undefined';
      chatObject.private = false;
      chatObject.users = users;
      chatObject.messages = [];
      chatObject.messages.push(messageObject);
      Meteor.call('initiateChat', chatObject);
    } else if (messagesInChat.endedAt === 'undefined') {
      Meteor.call('logMessage', messageObject);
    }

    e.target.sendMessage.value = '';
  }
});

Template.chatTabPanel.helpers({
  'chat': function () {
    var chatId = Session.get('activeChat');

    return ChatLogs.findOne({_id: chatId});
  },
  'fromCurrentUser': function (user) { // Proverava da li je poruka poslana od trenutnog korisnika ili od sagovornika
    var currentUser = Meteor.user().username;
    return currentUser === user;
  }
});

Template.registerHelper('createChatId', function (chatWithName) {
  var chatWith = Session.get('chatWith');
// Ne moze se uportrebiti Session.get('activeChat') jer se na osnovu ove funkcije
// prave linkovi koji kada se kliknu odredjuju koji je chat aktivan
  function isInChat (current) {
    return current.name === chatWithName;
  }
  chatWith = chatWith.filter(isInChat).map(function (current) {
    return current.chatId;
  });
  return chatWith.join('');
});
